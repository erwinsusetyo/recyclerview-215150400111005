package id.filkom.papb.recyclerview_215150400111005;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    TextView tvNIM, tvNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tvNIM = findViewById(R.id.tvNim2);
        tvNama = findViewById(R.id.tvNama2);

        String nim = getIntent().getStringExtra("nim");
        String nama = getIntent().getStringExtra("nama");

        tvNIM.setText(nim);
        tvNama.setText(nama);
    }
}